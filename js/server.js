
const start = () => {
   
    const PORT = 3000
    const http = require("http")

    http.createServer((req, res) => {

        res.write("Hello world!")
        res.end()
        
    }).listen(PORT, () => console.log("server started!"))
}

exports.start = start