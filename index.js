const readline = require('readline');
const server = require("./js/server")
const fs = require("fs")
const os = require("os")

const application = () => {

    console.log("\nChoose an option:")
    console.log("1. Read package.json")
    console.log("2. Display OS info")
    console.log("3. Start HTTP server")

    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    rl.question("Type a number: ", (number) => {

        switch (number) {
            case "1":
                readPackage()
                break
            case "2":
                displayOSInfo()
                break
            case "3":
                startServer()
                rl.close()
                return        
            default:
                console.log("Unvalid option!")
                break
        }

        rl.close();
        application()
    });

    
}


const readPackage = () => {
    console.log("Reading package.json file")
    const data = fs.readFileSync("./package.json", err => {
        if (err) throw err
        
    })
    console.log(JSON.parse(data))
}

const displayOSInfo = () => {
    console.log("Getting OS info")
    console.log("SYSTEM MEMORY: " + (os.totalmem() / 1000000000).toFixed(2) + " GB")
    console.log("FREE MEMORY: " + (os.freemem() / 1000000000).toFixed(2) + " GB")
    console.log("CPU CORES: " + os.cpus().length)
    console.log("ARCH: " + os.arch())
    console.log("PLATFORM: " + os.platform())
    console.log("RELEASE: " + os.release())
    console.log("USER: " + os.userInfo().username)
}

const startServer = () => {
    server.start()
}

application()